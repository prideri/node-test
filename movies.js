const koa_body = require('koa-body');
const Router = require('koa-router');
const MovieModel = require('./models/movies');

const router = new Router({
    prefix: '/movies',
});

router.get('/find', (ctx, next) => {
    ctx.body = MovieModel.find()
})

router.get('/find/:movie', (ctx, next) => {
    let movie = ctx.params.movie
    MovieModel.findById(movie, (err, movie) => {
        if (err) {
            ctx.response.status = 500
            ctx.body = err
        }
        if (!movie) {
            ctx.response.status = 404
            ctx.body = 'Movie not found'
        }
        ctx.response.status = 200
        ctx.body = movie
    })
})

router.post('/create', (ctx, next) => {
    console.log('POST /movies/create')
    const movie = new MovieModel()
    movie.title = ctx.request.body.title
    movie.year = ctx.request.body.year
    movie.released = ctx.request.body.released
    movie.genre = ctx.request.body.genre
    movie.director = ctx.request.body.director
    movie.actors = ctx.request.body.actors
    movie.plot = ctx.request.body.plot
    movie.ratings = ctx.request.body.ratings
    console.log('creando');
    movie.save((err, movieStored) => {
        console.log(movieStored)
        if (err) {
            ctx.response.status = 500
            ctx.body = err
        } else {
            ctx.response.status = 201;
            ctx.body = {
                status: 'success',
                message: movieStored
            };
        }
    })
});

router.post('/update', (ctx, next) => {
    if (checkPostFields(ctx).length > 0) {
        ctx.response.status = 404
        ctx.body = checkPostFields(ctx)
    } else {
        let movie = MovieModel.find({ title: ctx.request.body.movie })
        movie.plot = movie.plot.replace(ctx.request.body.find, ctx.request.body.replace)
        movie.save()
    }
    next()
})

function checkPostFields(ctx) {
    const notCtx = [];
    if (!ctx.request.body.movie) notCtx.push('field movie is required.');
    if (!ctx.request.body.find) notCtx.push('field find is required.');
    if (!ctx.request.body.replace) notCtx.push('field replace is required.');
    return notCtx;
}

module.exports = router;