'use strict'

const Koa = require('koa');
const mongoose = require('mongoose')
const koa_body = require('koa-body');
const app = new Koa();
let movies = require('./movies')

app.use(koa_body())
app.use(movies.routes());

mongoose.connect('mongodb://localhost:27017/movierep', (err, res) => {
    if (err) throw err
    console.log('Connection to mongodb success')

    app.listen(3000, function () {
        console.log('Servidor que se ejecuta en http://localhost:3000')
    });
})


