'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const MovieSchema = Schema({
    title: String,
    year: Number,
    released: Number,
    genre: {type: String, enum: ['action', 'drama', 'comedy', 'thriller', 'horror', 'animation']},
    director: String,
    actors: String,
    plot: String,
    ratings: Number
})

module.exports = mongoose.model('MovieModel', MovieSchema)